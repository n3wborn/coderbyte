
# CODERBYTE #

## Javascript Challenges and Solutions from [coderbyte](https://coderbyte.com/challenges) ##

This repo is intented to be for my personal use. I only want to keep every
challenges I made at [coderbyte](https://coderbyte.com/challenges).

Maybe later I will try to do the challenges in more than one langage but for
now I only do this to learn javascript.

Feel free to clone. I don't know about copyright and other horrible patents but
if I have to choose a license (or have to) then it will be GPL-v3.

